package game;
import java.util.*;

import player.*;


public class Game {
    public int realPlayerstotal;
    public List <Player> playerList = new ArrayList<Player>();
    public List <Dice> diceList = new ArrayList<Dice>();
    public String [] expression  = {"angry","happy","bitter","normal"};
    public Referee ref= new Referee("Ref");


    public Game(){
        
    }

    public void init(Scanner input){
        Random rand = new Random();
        System.out.println("Enter the number of real players:");
        realPlayerstotal= input.nextInt();
        for(int i=1;i<=realPlayerstotal;i++){
            Player player = getPlayerInfo(i,input);
            playerList.add(player);
        }
        for(int i=1;i<=4-realPlayerstotal;i++){
            int int_random = rand.nextInt(4);
            ComputerPlayer player = new ComputerPlayer("Computer "+i,expression[int_random]);
            playerList.add(player);
        }
        for(int i=1;i<=4;i++)diceList.add(setUpDice(i));
    }

    public Dice setUpDice(int i){
        int highProbFace= i;
        int []  otherFaces = new int[5];
        for(int j =1;j<=5;j++){
            if(j!=i)otherFaces[j-1]=j;
            else otherFaces[j-1]=j+1;
        }
        Dice dice= new Dice(highProbFace,otherFaces);
        return dice;
    }

    public Player getPlayerInfo(int i, Scanner input){
        System.out.println("Enter the name of player "+ i);
            String name = input.next();
            Realplayers player = new Realplayers(name);
            return player;
    }

    public void playgame(Scanner input){
        Random ran = new Random();
        ref.setUpOrder();
        boolean finish=false;
        Integer [] ord=ref.getOrder();
        int i=0;
        int score=0;
        while (!finish){
            if(i==4)i=0;
            if(playerList.get(ord[i]-1).isRealPlayer()){
                System.out.print("Player "+playerList.get(ord[i]-1).getName()+"'s turn(Roll or Quit):");
                String choice= input.next();
                if(choice.equals("Roll"))score= ref.rollDice(diceList.get(ran.nextInt(4)));
                else if(choice.equals("Quit")) {
                    finish=true;
                    break;
                }
            }
            else score= ref.rollDice(diceList.get(ran.nextInt(4)));
            playerList.get(ord[i]-1).setOldScore(playerList.get(ord[i]-1).getNewScore());
            int total=ref.calculateScore( playerList.get(ord[i]-1).getOldScore(),score);
            if (total!=21){
                System.out.println("Player "+playerList.get(ord[i]-1).getName()+" has rolled "+score);
                System.out.println("Current score: "+total);
            }
            playerList.get(ord[i]-1).setNewScore(total);
            if(total==21){
                ref.annouceWinners(playerList.get(ord[i]-1));
                if(playerList.get(ord[i]-1).isRealPlayer()){
                    for(Player players: playerList){
                        if(!players.isRealPlayer()){
                            ComputerPlayer comp = (ComputerPlayer) players;
                            System.out.println("Player "+comp.getName()+" feels "+ comp.getExperession());
                        }
                    }
                }
                finish=true;
            }
            i++;
        }
        System.out.println("Ending game");
    }
}
