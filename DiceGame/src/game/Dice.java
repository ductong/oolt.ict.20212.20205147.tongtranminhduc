package game;
public class Dice {
    private int highestProbFace;
    private int [] otherFaces= new int[5];
    
    
    public int getHighestProbFace() {
        return highestProbFace;
    }

    public int[] getOtherFaces() {
        return otherFaces;
    }
    public Dice(int highProbFace,int [] otherFaces){
        this.highestProbFace=highProbFace;
        this.otherFaces=otherFaces;
    }
    
}
