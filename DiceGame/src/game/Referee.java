package game;

import java.util.*;

import player.*;

public class Referee {
    private String name;
    private Integer [] order = new Integer [4];

    
    public String getName() {
        return name;
    }

    public Integer[] getOrder() {
        return order;
    }
    
    public Referee(String name){
        this.name = name;
    }

    public void setUpOrder(){
        Integer [] order ={1,2,3,4};
        List<Integer> ord =Arrays.asList(order);
        Collections.shuffle(ord);
        ord.toArray(order);
        this.order=order;
        System.out.println("Order of players:");
        System.out.println(Arrays.toString(order));
    }
    public int calculateScore(int oldScore,int newScore){
        int total =oldScore+ newScore;
        if(total>21)return 0;
        else return total;
    }
    public void annouceWinners(Player player){
        if (player.isRealPlayer()){
            System.out.println("The winner is "+player.getName());
            System.out.println("The score is: "+player.getNewScore());
            }
        else{
            System.out.println("The winner is "+player.getName());
            System.out.println("The score is: "+player.getNewScore());
        }
    }

    public int rollDice(Dice dice){
        int [] otherFace= dice.getOtherFaces();
        List<Integer> faces= new ArrayList<Integer>();
        for(int i=0;i<5;i++)for(int j=0;j<4;j++)faces.add(otherFace[i]);
        for(int i=0;i<5;i++)faces.add(dice.getHighestProbFace());
        Random rand = new Random();
        int index=rand.nextInt(25);
        return faces.get(index);
    }

    
}
 