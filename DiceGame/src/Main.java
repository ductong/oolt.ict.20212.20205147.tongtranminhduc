import java.util.Scanner;

import game.Game;

public class Main {
    public static void main(String [] args){
        boolean running= true;
        Scanner input = new Scanner(System.in);
        String choice;
        while (running){
            System.out.println("Enter what you want to do:");
            choice= input.next();
            if(choice.equals("Quit"))running=false;
            if(choice.equals("Play")){
                Game game= new Game();
                game.init(input);
                game.playgame(input);
            }
        }
        System.out.println("Quitting game");
        input.close();
    }
}
