package player;
public class ComputerPlayer extends Player {
    private String experession;

    public String getExperession() {
        return experession;
    }

    public ComputerPlayer(String name,String expression){
        super(name,false);
        this.experession= expression;
    }

}
