package player;
public class Player {
    
    private String name;
    private int newScore=0;
    private int oldScore=0;
    private int realPlayernum;
    private boolean isRealPlayer;

    public int getOldScore() {
        return oldScore;
    }
    
    public void setNewScore(int newScore) {
        this.newScore = newScore;
    }

    public void setOldScore(int oldScore) {
        this.oldScore = oldScore;
    }

    public String getName() {
        return name;
    }

    public boolean isRealPlayer() {
        return isRealPlayer;
    }

    public int getNewScore() {
        return newScore;
    }

    public int getRealPlayernum() {
        return realPlayernum;
    }


    public Player(String name,boolean isRealPlayer){
        this.name=name;
        this.isRealPlayer=isRealPlayer;
    }

}
