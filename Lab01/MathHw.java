import javax.swing.*;

public class MathHw {

    public static void main(String[] args) {
        String strNum1,strNum2;
        String notiSum= "The Sum is ";
        String notiDiff= "\nThe difference is ";
        String notiProd= "\nThe product is ";
        String notiQuot= "\nThe Quotient is ";
        strNum1=JOptionPane.showInputDialog(null,"Input the first number: ","Input 1st number:",JOptionPane.INFORMATION_MESSAGE);
        double num1=Double.parseDouble(strNum1);
        strNum2=JOptionPane.showInputDialog(null,"Input the second number: ","Input 2nd number:",JOptionPane.INFORMATION_MESSAGE);
        double num2=Double.parseDouble(strNum2);
        double sum= num1 + num2;
        double diff= num1 - num2;
        double product= num1 * num2;
        double quotient= num1 / num2;
        notiSum += sum; notiDiff += diff; notiProd += product; notiQuot += quotient;
        String noti= notiSum + notiDiff + notiProd + notiQuot;
        JOptionPane.showMessageDialog(null,noti,"The result",JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }
}