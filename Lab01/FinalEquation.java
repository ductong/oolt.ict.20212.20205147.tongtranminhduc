import javax.swing.*;

public class FinalEquation {

    public static void main(String[] args) {
        String choice;
        int num;
        do {
            choice = JOptionPane.showInputDialog(null, "Input your choice:", "Yourchoice", JOptionPane.INFORMATION_MESSAGE);
            num = Integer.parseInt(choice);
            if (num == 1) {
                String strA, strB;
                double numA, numB;
                strA = JOptionPane.showInputDialog(null, "Enter a:", "Choosing a", JOptionPane.INFORMATION_MESSAGE);
                numA = Double.parseDouble(strA);
                strB = JOptionPane.showInputDialog(null, "Enter b:", "Choosing b", JOptionPane.INFORMATION_MESSAGE);
                numB = Double.parseDouble(strB);
                double result = numB / numA;
                result = 0 - result;
                String answer = "The result is " + result;
                JOptionPane.showMessageDialog(null, answer, "The answer", JOptionPane.INFORMATION_MESSAGE);
            } else if (num == 2) {
                String A11, A12, A21, A22, B1, B2;
                int a11, a12, a21, a22, b1, b2;
                A11 = JOptionPane.showInputDialog(null, "Enter a11:", "Choosing a11", JOptionPane.INFORMATION_MESSAGE);
                a11 = Integer.parseInt(A11);
                A12 = JOptionPane.showInputDialog(null, "Enter a12:", "Choosing a12", JOptionPane.INFORMATION_MESSAGE);
                a12 = Integer.parseInt(A12);
                A21 = JOptionPane.showInputDialog(null, "Enter a21:", "Choosing a21", JOptionPane.INFORMATION_MESSAGE);
                a21 = Integer.parseInt(A21);
                A22 = JOptionPane.showInputDialog(null, "Enter a22:", "Choosing a22", JOptionPane.INFORMATION_MESSAGE);
                a22 = Integer.parseInt(A22);
                B1 = JOptionPane.showInputDialog(null, "Enter b1:", "Choosing b1", JOptionPane.INFORMATION_MESSAGE);
                b1 = Integer.parseInt(B1);
                B2 = JOptionPane.showInputDialog(null, "Enter b2:", "Choosing b2", JOptionPane.INFORMATION_MESSAGE);
                b2 = Integer.parseInt(B2);
                int D = a11 * a22 - a21 * a12;
                if (D == 0)
                    JOptionPane.showMessageDialog(null, "There is no solution to this problem", "The answer", JOptionPane.INFORMATION_MESSAGE);
                else {
                    int Dx = b1 * a22 - b2 * a12;
                    int Dy = a11 * b2 - b1 * a21;
                    double x = (double) Dx / (double) D;
                    double y = (double) Dy / (double) D;
                    String answer = "The result is " + x + " and " + y;
                    JOptionPane.showMessageDialog(null, answer, "The answer", JOptionPane.INFORMATION_MESSAGE);
                }
            } else if (num == 3) {
                String A, B, C;
                int a, b, c;
                A = JOptionPane.showInputDialog(null, "Enter a:", "Choosing a", JOptionPane.INFORMATION_MESSAGE);
                a = Integer.parseInt(A);
                B = JOptionPane.showInputDialog(null, "Enter b:", "Choosing b", JOptionPane.INFORMATION_MESSAGE);
                b = Integer.parseInt(B);
                C = JOptionPane.showInputDialog(null, "Enter c:", "Choosing c", JOptionPane.INFORMATION_MESSAGE);
                c = Integer.parseInt(C);
                int delta = b * b - 4 * a * c;
                if (delta < 0)
                    JOptionPane.showMessageDialog(null, "There are no solution", "The result", JOptionPane.INFORMATION_MESSAGE);
                else if (delta == 0) {
                    double result = -(double) b / (double) a;
                    String answer = "The result is " + result;
                    JOptionPane.showMessageDialog(null, answer, "The answer", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    double sqrtDelta = Math.sqrt(delta);
                    double x1 = (-(double) b + sqrtDelta) / ((double) a * 2);
                    double x2 = (-(double) b - sqrtDelta) / ((double) a * 2);
                    String answer = "The result is " + x1 + " and " + x2;
                    JOptionPane.showMessageDialog(null, answer, "The answer", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        } while (num < 4);
        System.exit(0);
    }
}
