package hust.soict.globalict.aims.media;
import java.util.*;

public class Book extends Media /*implements Comparable<Book>*/ {
    
    private List<String> authors = new ArrayList<String>();
    private String content;
    private List<String> contentTokens =new ArrayList<>();
    Map <String,Integer> wordFrequency = new TreeMap<>();
    
    public void processContent(){
        String[] tokens=this.content.split(" ");
        int num= tokens.length;
        for(int i=0;i<num;i++)contentTokens.add(tokens[i]);
        for(String i: contentTokens){
            if(wordFrequency.containsKey(i)){
                int val=wordFrequency.get(i);
                wordFrequency.put(i,val+1);
            }
            wordFrequency.putIfAbsent(i, 1);
        }
    }

    public String toString(){
        return this.getTitle()+" "+this.getCategory()+" "+this.getCost()+"$"+"\n"
        +"Length: "+contentTokens.size()+"\n"
        +"List of tokens = Word Frequencies:\n"+ wordFrequency;
    }

    public List<String> getAuthors() {
        return authors;
    }
    
    public void addAuthor(String authorName){
        for(String i:authors){
            if(authorName.equals(i)){
                System.out.println("This author already exist\n");
                return;
            }
        }
        authors.add(authorName);
    }

    public void removeAuthor(String authorName){
        int num=0;
        for(String i : authors){
            if(authorName.equals(i)){
                authors.remove(num);
                return;
            }
            num++;
        }
        System.out.println("Author does not exist");
    }

    public Book(String title){
        super(title);
    }
    public Book(String title, String category){
        super(title,category);
    }
    public Book(String title,String category,List<String> authors){
        super(title,category);
        this.authors=authors;
    }
    public Book(String title,String category,float cost,int id,List<String> authors){
        super(title,category,cost,id);
        this.authors=authors;
    }
    public Book(String title,String category,float cost,int id,String content){
        super(title,category,cost,id);
        this.content=content;
    }
    public int compareTo(Book item){
        return this.getTitle().compareTo(item.getTitle());
    }
    
}
