package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.Playable;

public class Track implements Playable, Comparable<Track>  {
    private String title;
    private int length;
    public String getTitle() {
        return title;
    }
    public int getLength() {
        return length;
    }
    public Track(String title){
        super();
        this.title=title;
    }
    public Track(String title, int length){
        super();
        this.title=title;
        this.length=length;
    }
    public void play() {
        System.out.println("Playing Track: " + this.getTitle());
        System.out.println("Track length: " + this.getLength());
        }
    public boolean equals(Track music){
        if(this.title.equals(music.getTitle()) & this.length==music.getLength())return true;
        return false;
    }
    public int compareTo(Track add){
        return this.getTitle().compareTo(add.getTitle());
    }
}
