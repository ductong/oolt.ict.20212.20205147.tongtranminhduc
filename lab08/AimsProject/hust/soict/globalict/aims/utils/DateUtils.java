package hust.soict.globalict.aims.utils;

import javax.xml.parsers.ParserConfigurationException;
import java.text.*;
import java.util.*;

public class DateUtils {
    public static void comparingDates() throws ParserConfigurationException, ParseException {
        try (Scanner input = new Scanner(System.in)) {
            System.out.println("Enter the first date(dd/mm/yyyy):");
            String day1= input.nextLine();
            System.out.println("Enter the second date(dd/mm/yyyy):");
            String day2=input.nextLine();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date d1= sdf.parse(day1);
            Date d2= sdf.parse(day2);
            if(d1.compareTo(d2) > 0) {
                System.out.println("Date 1 occurs after Date 2");
            } else if(d1.compareTo(d2) < 0) {
                System.out.println("Date 1 occurs before Date 2");
            } else if(d1.compareTo(d2) == 0) {
                System.out.println("Both dates are equal");
            }
        }
    }
    public static void sortingDates() throws ParseException {
        try (Scanner input = new Scanner(System.in)) {
            System.out.println("Enter a number a of dates:");
            int num= input.nextInt();
            String [] dates= new String[num];
            input.nextLine();
            for(int i= 0;i<num;i++){
                int d=i+1;
                System.out.println("Enter the date " +d+" (dd/mm/yyyy):");
                dates[i]= input.nextLine();
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date [] d= new Date[num];
            for(int i=0;i<num;i++)d[i]=sdf.parse(dates[i]);
            for(int i = 0;i<num;i++){
                for(int j =0;j<num;j++){
                    if(d[i].compareTo(d[j]) < 0){
                        Date temp= d[i];
                        d[i]=d[j];
                        d[j]=temp;
                    }
                }
            }
            sdf.applyPattern("dd/MM/yyyy");
            for(int i=0;i<num;i++)dates[i]= sdf.format(d[i]);
            for(int i=0;i<num;i++) System.out.println(dates[i]);
        }
    }

}
