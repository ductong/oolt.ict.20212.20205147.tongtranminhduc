package hust.soict.globalict.test.media;

import java.util.Scanner;

import hust.soict.globalict.aims.media.Book;

public class BookTest {
    public static void main (String [] args){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter content:");
        String content= input.nextLine();
        Book item =new Book("Cinderella","Tale",54f,123,content);
        input.close();
        item.processContent();
        System.out.println(item.toString());
    }
    
}
