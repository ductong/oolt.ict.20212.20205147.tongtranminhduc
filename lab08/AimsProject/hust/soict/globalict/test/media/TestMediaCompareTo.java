package hust.soict.globalict.test.media;
import java.util.*;


import hust.soict.globalict.aims.media.DigitalVideoDisc;

public class TestMediaCompareTo {
    public static void main(String[] args){
        Collection<DigitalVideoDisc> collection = new ArrayList<DigitalVideoDisc>();
        DigitalVideoDisc dvd1= new DigitalVideoDisc("The lion king","Animated",45f,3423);
        DigitalVideoDisc dvd2= new DigitalVideoDisc("Frozen","Animated",64f,4324);
        DigitalVideoDisc dvd3= new DigitalVideoDisc("Mission Impossible","Action",5f,489);
        collection.add(dvd1);
        collection.add(dvd2);
        collection.add(dvd3);
        /*CompactDisc cd1= new CompactDisc("Taylor Swift");
        CompactDisc cd2= new CompactDisc("Sam Smith");
        CompactDisc cd3= new CompactDisc("Maxwell Blaze");
        collection.add(cd1);
        collection.add(cd2);
        collection.add(cd3);
        Book bk1= new Book("Hello Bro");
        Book bk2= new Book("Toan 12");
        Book bk3= new Book("Ngu van 11");
        Book bk4= new Book("Tieng Anh");
        collection.add(bk1);
        collection.add(bk2);
        collection.add(bk3);
        collection.add(bk4);*/
        Iterator<DigitalVideoDisc> iterator = collection.iterator();
        System.out.println("-----------------------------------");
        System.out.println("The DVDs currently in the order are:");
        while(iterator.hasNext()){
            System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
        }
        Collections.sort((List<DigitalVideoDisc>) collection);
        iterator = collection.iterator();
        System.out.println("-----------------------------------");
        System.out.println("The DVDs in sorted order order are:");        
        while(iterator.hasNext()){
            System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
        }
        System.out.println("-----------------------------------");

    }
    
;}
