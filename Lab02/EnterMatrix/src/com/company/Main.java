package com.company;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        int width,length;
        Scanner input=new Scanner(System.in);
        System.out.println("Enter the width of the matrix:");
        width=input.nextInt();
        System.out.println("Enter the length of the matrix:");
        length=input.nextInt();
        int mat1[][]=new int[length][width];
        for(int i=0;i<length;i++){
            for(int j=0;j<width;j++){
                System.out.println("Enter the a"+i+j+" element of matrix 1:");
                mat1[i][j]=input.nextInt();
            }
        }
        int mat2[][]=new int[length][width];
        for(int i=0;i<length;i++){
            for(int j=0;j<width;j++){
                System.out.println("Enter the a"+i+j+" element of matrix 2:");
                mat2[i][j]=input.nextInt();
            }
        }
        System.out.println("Matrix 1:\n");
        System.out.println(Arrays.deepToString(mat1));
        System.out.println("Matrix 2:\n");
        System.out.println(Arrays.deepToString(mat2));
        System.exit(0);
    }
}
