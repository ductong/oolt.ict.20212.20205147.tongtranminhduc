package hust.soict.globalict.lab02.DayMonth;
import java.util.Objects;
import java.util.Scanner;


public class DayMonth {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String monthName = null;
        System.out.println("Enter the month(Numbers,Words,Abbreviation):");
        String month = input.nextLine();
        int year;
        do {
            System.out.println("Enter the year:");
            year = input.nextInt();
        } while (year < 0 & year >2022 );
        int dayMonth = 0;
        if (Objects.equals("Jan", month) | Objects.equals("1", month) | Objects.equals("January", month)|Objects.equals("Jan.", month)) {
            monthName = "January";
            dayMonth = 31;
        } else if (Objects.equals("Mar", month) | Objects.equals("3", month) | Objects.equals("March", month)|Objects.equals("Mar.", month)) {
            monthName = "March";
            dayMonth = 31;
        } else if (Objects.equals("Apr", month) | Objects.equals("4", month) | Objects.equals("April", month)|Objects.equals("Apr.", month)) {
            monthName = "April";
            dayMonth = 30;
        } else if (Objects.equals("May", month) | Objects.equals("5", month)) {
            monthName = "May";
            dayMonth = 31;
        } else if (Objects.equals("Jun", month) | Objects.equals("6", month) | Objects.equals("June", month)) {
            monthName = "June";
            dayMonth = 30;
        } else if (Objects.equals("Jul", month) | Objects.equals("7", month) | Objects.equals("July", month)) {
            monthName = "July";
            dayMonth = 31;
        } else if (Objects.equals("Aug", month) | Objects.equals("8", month) | Objects.equals("August", month)|Objects.equals("Aug.", month)) {
            monthName = "August";
            dayMonth = 31;
        } else if (Objects.equals("Sep", month) | Objects.equals("9", month) | Objects.equals("September", month)|Objects.equals("Sept.", month)) {
            monthName = "September";
            dayMonth = 30;
        } else if (Objects.equals("Oct", month) | Objects.equals("10", month) | Objects.equals("October", month)|Objects.equals("Aug.", month)) {
            monthName = "October";
            dayMonth = 31;
        } else if (Objects.equals("Nov", month) | Objects.equals("11", month) | Objects.equals("November", month)|Objects.equals("Nov.", month)) {
            monthName = "November";
            dayMonth = 30;
        } else if (Objects.equals("Dec", month) | Objects.equals("12", month) | Objects.equals("December", month)|Objects.equals("Dec.", month)) {
            monthName = "December";
            dayMonth = 31;
        } else if (Objects.equals("Feb", month) | Objects.equals("2", month) | Objects.equals("February", month)) {
            monthName = "February";
            if (year % 4 == 0) {
                if (year % 400 == 0) dayMonth = 29;
                else if (year % 100 == 0) dayMonth = 28;
                else dayMonth = 29;
            } else dayMonth = 28;
        }
        System.out.println("Month: "+ monthName +",number of days: "+ dayMonth + ", year: "+year);
        input.close();
        System.exit(0);
    }
}
