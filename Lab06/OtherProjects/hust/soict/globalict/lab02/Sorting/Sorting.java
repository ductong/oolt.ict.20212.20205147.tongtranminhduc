package hust.soict.globalict.lab02.Sorting;
import java.util.*;

public class Sorting {

    public static void main(String[] args) {
        Scanner input= new Scanner(System.in);
        System.out.println("Enter the array length:");
        int len=input.nextInt();
        int [] arr = new int[len];
        for(int i=0;i<len;i++){
            System.out.println("Enter the element "+(i+1)+":");
            arr[i]=input.nextInt();
        }
        for(int i=0;i<len;i++) {
            int temp;
            for(int j=i+1;j<len;j++){
                if(arr[j]<arr[i]){
                    temp=arr[i];
                    arr[i]=arr[j];
                    arr[j]=temp;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
        input.close();
        System.exit(0);
    }
}
