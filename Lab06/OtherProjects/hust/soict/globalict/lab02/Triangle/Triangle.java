package hust.soict.globalict.lab02.Triangle;
import java.util.Scanner;

public class Triangle {

    public static void main(String[] args) {
        Scanner input= new Scanner(System.in);
        System.out.println("Enter the height of the triangle");
        int n= input.nextInt();
        for(int i=0; i<n; i++)
        {
            for(int j=n; j>=i; j--)
            {
                System.out.print(" ");
            }
            for(int x=0; x<=(2*i); x++)
            {
                System.out.print("*");
            }
            for(int k=n; k>=i; k--)
            {
                System.out.print(" ");
            }
            System.out.print("\n");
        }
        input.close();
    }
}
