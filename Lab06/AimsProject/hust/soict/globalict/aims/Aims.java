package hust.soict.globalict.aims;

import java.util.*;

import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.order.Order;

public class Aims {

    public static void main(String[] args) {
        Scanner input= new Scanner(System.in);
        Order anOrder[]= new Order[5];
        int curOrder=0;
        while(true){
                    System.out.println("Order Management Application: ");
                    System.out.println("--------------------------------");
                    System.out.println("1. Create new order");
                    System.out.println("2. Add item to the order");
                    System.out.println("3. Delete item by id");
                    System.out.println("4. Display the items list of order");
                    System.out.println("0. Exit");
                    System.out.println("--------------------------------");
                    System.out.println("Please choose a number: 0-1-2-3-4");
                    System.out.print("Enter option:");
                    int inp = input.nextInt();
                    if(inp==0){
                        System.out.println("Closing system\n");
                        break;
                    }
                    else if(inp == 1){
                        if(Order.update()){
                            curOrder++;
                            anOrder[curOrder-1]=new Order();
                            System.out.println("Order created");
                        }
                        else System.out.println("Limit number of orders reached");
                    }
                    else if(inp ==2){
                        System.out.println("How many items do you want to add:");
                        int numItem=input.nextInt();
                        for(int i = 0;i<numItem;i++){ 
                            Media item = new Media();   
                            System.out.println("Enter the title of item "+ i+": ");
                            item.setTitle(input.next());
                            input.nextLine();
                            System.out.println("Enter the category of item "+ i+":");
                            item.setCategory(input.next());
                            input.nextLine();
                            System.out.println("Enter the cost of item "+i+":");
                            item.setCost(input.nextFloat());
                            System.out.println("Enter the Id of the item "+ i+":");
                            item.setId(input.nextInt());
                            anOrder[curOrder-1].addMedia(item);
                        }
                    }
                    else if(inp ==3){
                        System.out.println("Enter the id of the item to delete:");
                        int del= input.nextInt();
                        anOrder[curOrder-1].deleteById(del);
                    }
                    else if(inp==4){
                        anOrder[curOrder-1].printOrder();
                    }
                }
                input.close();
            }
}
