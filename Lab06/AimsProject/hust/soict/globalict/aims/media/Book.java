package hust.soict.globalict.aims.media;
import java.util.*;

public class Book extends Media {
    
    private List<String> authors = new ArrayList<String>();
    
    public List<String> getAuthors() {
        return authors;
    }
    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }
    
    public void addAuthor(String authorName){
        for(String i:authors){
            if(authorName.equals(i)){
                System.out.println("This author already exist\n");
                return;
            }
        }
        authors.add(authorName);
    }

    public void removeAuthor(String authorName){
        int num=0;
        for(String i : authors){
            if(authorName.equals(i)){
                authors.remove(num);
                return;
            }
            num++;
        }
        System.out.println("Author does not exist");
    }
    
}
