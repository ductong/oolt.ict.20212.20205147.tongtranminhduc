package hust.soict.globalict.test.disc;

import hust.soict.globalict.aims.media.DigitalVideoDisc;

public class TestingPassingParameter {
    public static void main(String[] args) {
        DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
        DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");

        System.out.println("jungle dvd title:" + jungleDVD.getTitle());
        System.out.println("cinderella dvd title:" + cinderellaDVD.getTitle());

        swap(jungleDVD,cinderellaDVD);

        System.out.println("jungle dvd title:" + jungleDVD.getTitle());
        System.out.println("cinderella dvd title:" + cinderellaDVD.getTitle());

        /**changeTitle(jungleDVD, cinderellaDVD.getTitle());
        System.out.println("jungle dvd title:" + jungleDVD.getTitle());**/
    }

    /**
     * public static void swap(Object o1,Object o2){
     * Object tmp=o1;
     * o1=o2;
     * o2=tmp;
     * }
     **/
    /**public static void changeTitle(DigitalVideoDisc dvd, String title) {
        String oldTitle = dvd.getTitle();
        dvd.setTitle(title);
    }**/

    public static void swap(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2){
        DigitalVideoDisc tmp=new DigitalVideoDisc("");
        tmp =passValue(tmp,dvd1);
        dvd1=passValue(dvd1,dvd2);
        dvd2=passValue(dvd2,tmp);
    }

    public static DigitalVideoDisc passValue(DigitalVideoDisc from,DigitalVideoDisc into){
        from.setTitle(into.getTitle());
        from.setCost(into.getCost());
        from.setCategory(into.getCategory());
        from.setDirector(into.getDirector());
        from.setLength(into.getLength());
        return from;
    }
}
