package hust.soict.globalict.aims.media;

public class DigitalVideoDisc extends Media {
    private String director;
    private int length;

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public DigitalVideoDisc(String title) {
        super();
        this.setTitle(title);;
    }

    public DigitalVideoDisc(String category, String title){
        super();
        this.setCategory(category);
        this.setTitle(title);
    }

    public DigitalVideoDisc(String title, String category, String director) {
        super();
        this.setTitle(title);
        this.setCategory(category);
        this.director = director;
    }

    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        super();
        this.setTitle(title);
        this.setCategory(category);
        this.director = director;
        this.length = length;
        this.setCost(cost);
    }

    public boolean search(String title){
        String [] token = title.split(" ");
        int n= token.length;
        for(int i= 0 ;i<n;i++){
            if(this.getTitle().contains(token[i])==false)return false;
        }
        return true;
    }

}


