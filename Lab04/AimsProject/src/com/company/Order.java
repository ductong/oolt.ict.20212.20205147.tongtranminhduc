package com.company;

public class Order {
    public static final int MAX_NUMBERS_ORDERED = 10;
    public static final int MAX_LIMITED_ORDERS = 5;
    private static int nbOrders =0;
    private DigitalVideoDisc itemOrdered[]= new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
    int qtyOrdered=0;
    private MyDate dateOrdered = new MyDate();

    public static boolean update(){
        if(nbOrders==MAX_LIMITED_ORDERS){
            System.out.println("Maximum number of orders reached");
            return false;
        }
        else nbOrders++;
        return true;
    }

    public void printOrder(){
        System.out.println("***********************Order***********************\n");
        System.out.println("Date:");dateOrdered.printDay();
        System.out.println(nbOrders+"\n");
        System.out.println("\nOrdered items:\n");
        for(int i=0;i<qtyOrdered;i++){
            int u=i+1;
            System.out.println(u+"."+itemOrdered[i].getTitle()+"-"+itemOrdered[i].getCategory()+"-"+itemOrdered[i].getDirector()+"-"+itemOrdered[i].getLength()+":"+itemOrdered[i].getCost()+" $\n");
        }
        System.out.println("Total cost:"+totalCost()+"\n");
        System.out.println("***************************************************\n");
    }

    public void addDigitalVideoDics(DigitalVideoDisc dics){
        if(qtyOrdered==MAX_NUMBERS_ORDERED){
            System.out.println("The order is almost full\n");
            return;
        }
        else {
            itemOrdered[qtyOrdered]=dics;
            qtyOrdered++;
        }
        }
    public void removeDigitalVideoDics(DigitalVideoDisc dics){
        if(qtyOrdered==0){
            System.out.println("The order is empty");
            return;
        }
        else {
            for(int i=0;i<qtyOrdered;i++){
                if(itemOrdered[i]==dics){
                    for(int j=i;j<qtyOrdered-1;j++)itemOrdered[j]=itemOrdered[j+1];
                    qtyOrdered--;
                    return;
                }
                if(i==qtyOrdered-1)System.out.println("Item is not added");
            }
        }
    }
    public float totalCost(){
        float total=0;
        for(int i=0;i<qtyOrdered;i++)total=total+itemOrdered[i].getCost();
        return total;
    }
    public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList){
        int num= dvdList.length;
        if(qtyOrdered+num>MAX_NUMBERS_ORDERED){
            System.out.println("The limit number of orders is reached");
            return;
        }
        else {
            for(int i=0;i<num;i++){
                itemOrdered[qtyOrdered]=dvdList[i];
                qtyOrdered++;
            }
        }
    }
    public void addDigitalVideoDisc(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2,DigitalVideoDisc dvd3){
        if(qtyOrdered+3>MAX_NUMBERS_ORDERED){
            System.out.println("Limit number of orders reached");
            return;
        }
        else{
            itemOrdered[qtyOrdered]=dvd1;
            qtyOrdered++;
            itemOrdered[qtyOrdered]=dvd2;
            qtyOrdered++;
            itemOrdered[qtyOrdered]=dvd3;
            qtyOrdered++;
        }
    }
    public void addDigitalVideoDisc(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2) {
        if (qtyOrdered + 2 > MAX_NUMBERS_ORDERED) {
            System.out.println("Limit number of orders reached");
            return;
        } else {
            itemOrdered[qtyOrdered] = dvd1;
            qtyOrdered++;
            itemOrdered[qtyOrdered] = dvd2;
            qtyOrdered++;
        }
    }
}
