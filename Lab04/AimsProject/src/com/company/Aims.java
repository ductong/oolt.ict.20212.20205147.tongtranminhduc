package com.company;

public class Aims {

    public static void main(String[] args) {
        if(Order.update()) {
            Order anOrder = new Order();
            DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
            dvd1.setCategory("Animation");
            dvd1.setCost(19.95f);
            dvd1.setDirector("Roger Allers");
            dvd1.setLength(87);

            DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
            dvd2.setCategory("Science Fiction");
            dvd2.setCost(24.95f);
            dvd2.setDirector("George Lucas");
            dvd2.setLength(124);

            DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladin");
            dvd3.setCategory("Animation");
            dvd3.setCost(18.99f);
            dvd3.setDirector("John Musker");
            dvd3.setLength(90);

            DigitalVideoDisc[] dvdList = new DigitalVideoDisc[3];
            dvdList[0] = dvd1;
            dvdList[1] = dvd2;
            dvdList[2] = dvd3;
            anOrder.addDigitalVideoDisc(dvdList);

            anOrder.printOrder();
        }
        if(Order.update()) {
            Order anOrder1 = new Order();
            DigitalVideoDisc dvd4 = new DigitalVideoDisc("The Lion King");
            dvd4.setCategory("Animation");
            dvd4.setCost(19.95f);
            dvd4.setDirector("Roger Allers");
            dvd4.setLength(87);

            DigitalVideoDisc dvd5 = new DigitalVideoDisc("Star Wars");
            dvd5.setCategory("Science Fiction");
            dvd5.setCost(24.95f);
            dvd5.setDirector("George Lucas");
            dvd5.setLength(124);

            DigitalVideoDisc dvd6 = new DigitalVideoDisc("Aladin");
            dvd6.setCategory("Animation");
            dvd6.setCost(18.99f);
            dvd6.setDirector("John Musker");
            dvd6.setLength(90);

            DigitalVideoDisc[] dvdList1 = new DigitalVideoDisc[3];
            dvdList1[0] = dvd4;
            dvdList1[1] = dvd5;
            dvdList1[2] = dvd6;
            anOrder1.addDigitalVideoDisc(dvdList1);

            anOrder1.printOrder();
        }

        /**System.out.print("The total cost is :");
        System.out.println(anOrder.totalCost());**/
    }
}
