package com.company;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.lang.*;
import java.util.*;

public class MyDate {
    private int day;
    private int month;
    private int year;

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    public MyDate(){
        LocalDate currentDate = LocalDate.now();
        this.day=currentDate.getDayOfMonth();
        this.month=currentDate.getMonthValue();
        this.year=currentDate.getYear();
    }
    public MyDate(int day,int month,int year){
        this.day=day;
        this.month=month;
        this.year=year;
    }
    public MyDate(String date){
        int day,month,year;
        month=0;
        String [][] name={{"January","1"},{"February","2"},{"March","3"},{"April","4"},{"May","5"},{"June","6"},{"July","7"},
                {"August","8"},{"September","9"},{"October","10"},{"November","11"},{"December","12"}};
        String[] parts=date.split(" ");
        for(int i=0;i<12;i++){
            if(parts[0].equals(name[i][0])){
                month=Integer.parseInt(name[i][1]);
                break;
            }
            if(i==11){
                System.out.println("Invalid month name");
                return;
            }
        }
        year=Integer.parseInt(parts[2]);
        if(year<=0||year>2022){
            System.out.println("Invalid Year");
            return;
        }
        String num = parts[1].replaceAll("[^0-9]","");
        day = Integer.parseInt(num);
        if(day>31){
            System.out.println("Invalid Day");
            return;
        }
        else if(month==4|month==6|month==9|month==11){
            if(day>30){
                System.out.println("Invalid Day");
                return;
            }
        }
        else if(month ==2){
            int dayMonth;
            if (year % 4 == 0) {
                if (year % 400 == 0) dayMonth = 29;
                else if (year % 100 == 0) dayMonth = 28;
                else dayMonth = 29;
            } else dayMonth = 28;
            if(day>dayMonth){
                System.out.println("Invalid Day");
                return;
            }
        }
        this.day=day;
        this.month=month;
        this.year=year;
    }
    public static String accept(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the Date:\n");
        String date=input.nextLine();
        return date;
    }
    public MyDate(String day,String month,String year){
        int nam;
        String [][] monthName={{"January","1"},{"February","2"},{"March","3"},{"April","4"},{"May","5"},{"June","6"},{"July","7"},
                {"August","8"},{"September","9"},{"October","10"},{"November","11"},{"December","12"}};
        String [][] dayName={{"First","1"},{"Second","2"},{"Third","3"},{"Fourth","4"},{"Fifth","5"},{"Sixth","6"},{"Seventh","7"},
                {"Eighth","8"},{"Ninth","9"},{"Tenth","10"},{"Eleventh","11"},{"Twelfth","12"},{"Thirteenth","13"},{"Fourteenth","14"},
                {"Fifteenth","15"},{"Sixteenth","16"},{"Seventeenth","17"},{"Eighteenth","18"},{"Nineteenth","19"},{"Twentieth","20"},
                {"Twenty-first","21"},{"Twenty second","22"},{"Twenty-third","23"},{"Twenty-fourth","24"},{"Twenty fifth","25"},{"Twentysixth","26"},
                {"Twentyseventh","27"},{"Twentyeighth","28"},{"Twentyninth","29"},{"Thirtieth","30"},{"Thirtyfirst","31"}};
        for(int i=0;i<12;i++){
            if(month.equals(monthName[i][0])){
                this.month=Integer.parseInt(monthName[i][1]);
                break;
            }
            if(i==11){
                System.out.println("Invalid month name");
                return;
            }
        }
        for(int i=0;i<30;i++){
            if(day.equals(dayName[i][0])){
                this.day=Integer.parseInt(dayName[i][1]);
                break;
            }
            if(i==30){
                System.out.println("Invalid day");
                return;
            }
        }
        nam = Integer.parseInt(year);
        if(nam >2022) System.out.println("Invalid year");
        this.year=nam;
    }
    public void chooseFormat(String format)throws Exception{
        String sdate=this.day+"/"+this.month+"/"+this.year;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date d = sdf.parse(sdate);
        sdf.applyPattern(format);
        sdate = sdf.format(d);
        System.out.println(sdate);
    }
    public void printDay(){
        System.out.println(this.day+"-"+this.month+"-"+this.year);
    }
    public static void print() throws ParseException {
        LocalDate currentDate = LocalDate.now();
        int day=currentDate.getDayOfMonth();
        int month=currentDate.getMonthValue();
        int year=currentDate.getYear();
        String sdate=day+"/"+month+"/"+year;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date d = sdf.parse(sdate);
        sdf.applyPattern("MMM dd");
        String sdate1 = sdf.format(d);
        System.out.printf(sdate1);
        if(day==1|day==11|day==21|day==31){
            System.out.printf("st");
        }
        else if(day==2|day==12|day==22){
            System.out.printf("nd");
        }
        else if(day==3|day==13|day==23){
            System.out.printf("rd");
        }
        else System.out.printf("th");
        sdf.applyPattern(" yyyy");
        sdate1 = sdf.format(d);
        System.out.printf(sdate1);
    }
}
